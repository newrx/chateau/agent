# IoT Agent

Runs on the embedded (raspberry pi) LoRa gateway for a home. May run in a cluster as leader or follower, or in single node mode. Offers an AP if not connected to Wifi or Ethernet, sends keepalives and status to the cloud, listens for commands from the cloud and local devices on the network (phones, pcs, other nodes in clustered mode).

Helpful links:
+ https://godoc.org/github.com/rtr7/router7/internal/dhcp4d
+ https://godoc.org/github.com/rtr7/router7/internal/dns
+ https://godoc.org/github.com/rtr7/router7/internal/netconfig
+ https://github.com/solarnz/wireless
+ https://github.com/wiless/gocomm
+ https://github.com/theojulienne/go-wireless