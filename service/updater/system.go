package updater

// ota provides mechanisms for safe ota updates to agent.
// ota keeps the last version and current version stored in
// /opt/agent/avm/versions and uses a symlink to point at the
// currently used version. when updating, the new version will
// be downloaded to the avm dir and symlinked automatically.
// if the new version fails over, bootstrapd will kill the new
// version and symlink back to the old.
