package indicator

type IndicatorService struct{}
type LedConfig struct{}

func New(conf *LedConfig) (*IndicatorService, error) {
	return nil, nil
}

func (isrv *IndicatorService) Init() error {
	return nil
}

func (isrv *IndicatorService) Destroy() error {
	return nil
}

func (isrv *IndicatorService) Deps() []string {
	return nil
}