package persist

type Record struct{
	Name string
	Obj interface{}
}

type Instance struct{}
type Opts struct{}

func Init(opts *Opts) (*Instance, error) {
	return nil, nil
}

func (i *Instance) Save(records ...Record) (uint32, error) {
	return 0, nil
}

func (i *Instance) Clear() error {
	return nil
}

func (i *Instance) Get() (error) {
	return nil
}