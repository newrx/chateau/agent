package service

type Service interface{
	Init() error
	Destroy() error
	Deps() []string
}