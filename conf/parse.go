package conf

import (
	"io/ioutil"

	"github.com/BurntSushi/toml"
)

type Config struct {
	CloudEndpoints string `toml:"cloud_endpoints"`
}

func Parse(inputfile string) (*Config, error) {
	data, err := ioutil.ReadFile(inputfile)
	if err != nil {
		return nil, err
	}

	config := Config{}
	err = toml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
