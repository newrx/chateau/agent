package service

import "github.com/spf13/cobra"

func Cmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "service",
		Short: "manage an agent service",
	}

	cmd.AddCommand(StartCmd())
	cmd.AddCommand(StopCmd())

	return cmd
}
