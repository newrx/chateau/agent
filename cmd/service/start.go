package service

import (
	"fmt"

	"github.com/spf13/cobra"
)

func StartCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "start",
		Short: "start the agent service",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("agent started")
		},
	}
}
