package service

import (
	"fmt"

	"github.com/spf13/cobra"
)

func StopCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "stop",
		Short: "stop the agent service",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("agent stopped")
		},
	}
}
