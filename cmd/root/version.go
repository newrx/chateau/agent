package root

import (
	"fmt"

	"github.com/spf13/cobra"
)

func VersionCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "show the agent version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("chateau agent v1.0.0")
		},
	}
}
