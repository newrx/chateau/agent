package root

import (
	"fmt"

	"github.com/spf13/cobra"
)

func EnvCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "env",
		Short: "show the agent environment information",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("env not yet setup")
			// TODO: add hosted-cloud-edition or community-cloud-edition
		},
	}
}
