package main

import (
	"fmt"
	"os"
	"path"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"

	"gitlab.com/rucuriousyet/agent/cmd/root"
	"gitlab.com/rucuriousyet/agent/cmd/service"
)

func main() {
	subcmds := []*cobra.Command{
		service.Cmd(),
		root.EnvCmd(),
		root.VersionCmd(),
	}

	cmd := &cobra.Command{
		Use:   "agent",
		Short: "chateau agent",
	}

	cmd.AddCommand(subcmds...)
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defaultConfigPath := path.Join(home, ".agent/config.toml")
	cmd.PersistentFlags().String("config", defaultConfigPath, "sets the config file")

	if err := cmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
